#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}

    //printf("Opened file");
	
	// TODO
    
	// Allocate memory for an array of strings (arr).
    char **arr = malloc( 10*sizeof(char) );

	// Read the dictionary line by line.
    int counter = 0;
    char line[100];
    int arrLength = 10;
    while (fgets(line, 100, in) != NULL){
        // Removes \n from input (comes from fgets)
        char *nl = strchr(line, '\n');
        if (nl) *nl = '\0';

        // Expand array if necessary (realloc).
        if (counter == arrLength){
            arrLength += 5;
            arr = realloc( arr, arrLength * sizeof(char*));
        }

        // Allocate memory for the string (str).
        char *str = malloc(strlen(line)+1);

        // Copy each line into the string (use strcpy).
        strcpy( str, line);

        // Attach the string to the large array (assignment =).
        arr[counter] = str;

        //printf("arr[%d] = :%s: || actual: :%s:\n", counter, line, arr[counter]);
        counter++;
    }
	
	// The size should be the number of entries in the array.
	*size = counter;
	
	// Return pointer to the array of strings.
	return arr;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL) return NULL;
    
	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
	        return dictionary[i];
	    }
	}
	return NULL;
}